export default () => {
    console.log('Hello entry point B');
    return import(/* webpackChunkName: "lib-a" */ './libs/lib-a').then(({ default: sayHello }) => sayHello());
};