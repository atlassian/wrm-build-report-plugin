# 1.3.1

## Changed
* Bumped `atlassian-wrm-dependency-analyzer` to v1.1.0. The package is now significantly lighter.

# 1.3.0

## Changed
* `filename` and `staticReportFilename` must be absolute file paths.
